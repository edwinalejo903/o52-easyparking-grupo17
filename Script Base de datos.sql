-- MySQL Script generated by MySQL Workbench
-- Sun Sep 11 07:47:44 2022
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema EasyParking
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema EasyParking
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `EasyParking` DEFAULT CHARACTER SET utf8 ;
USE `EasyParking` ;

-- -----------------------------------------------------
-- Table `EasyParking`.`TipoVehiculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EasyParking`.`TipoVehiculo` (
  `idTipoVehiculo` INT NOT NULL AUTO_INCREMENT,
  `TipoVehiculo` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idTipoVehiculo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EasyParking`.`Tarifa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EasyParking`.`Tarifa` (
  `idTarifa` INT NOT NULL AUTO_INCREMENT,
  `valorMinuto` INT NOT NULL,
  `idTipoVehiculo` INT NOT NULL,
  PRIMARY KEY (`idTarifa`),
  UNIQUE INDEX `idTarifa_UNIQUE` (`idTarifa` ASC) VISIBLE,
  INDEX `fk_TipoVehiculo_Tarifa_idx` (`idTipoVehiculo` ASC) VISIBLE,
  CONSTRAINT `fk_TipoVehiculo_Tarifa`
    FOREIGN KEY (`idTipoVehiculo`)
    REFERENCES `EasyParking`.`TipoVehiculo` (`idTipoVehiculo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EasyParking`.`Plaza`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EasyParking`.`Plaza` (
  `idPlaza` INT NOT NULL AUTO_INCREMENT,
  `Disponible` INT NOT NULL,
  `idTarifa` INT NOT NULL,
  `Codigo` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idPlaza`),
  INDEX `FK_Tarifa_Plaza_idx` (`idTarifa` ASC) VISIBLE,
  CONSTRAINT `FK_Tarifa_Plaza`
    FOREIGN KEY (`idTarifa`)
    REFERENCES `EasyParking`.`Tarifa` (`idTarifa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EasyParking`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EasyParking`.`Usuario` (
  `idUsuario` INT NOT NULL AUTO_INCREMENT,
  `nombreCompleto` VARCHAR(45) NOT NULL,
  `celular` INT NULL,
  `cedula` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE INDEX `Nombre_UNIQUE` (`nombreCompleto` ASC) VISIBLE,
  UNIQUE INDEX `cedula_UNIQUE` (`cedula` ASC) VISIBLE)
ENGINE = InnoDB
COMMENT = '	';


-- -----------------------------------------------------
-- Table `EasyParking`.`Detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EasyParking`.`Detalle` (
  `idDetalle` INT NOT NULL AUTO_INCREMENT,
  `fechaIngreso` DATE NOT NULL,
  `horaIngreso` DATE NOT NULL,
  `fechaSalida` DATE NULL,
  `horaSalida` DATE NULL,
  `tarifa` INT NULL,
  `idUsuario` INT NOT NULL,
  `idPlaza` INT NOT NULL,
  `Placa` VARCHAR(6) NOT NULL,
  PRIMARY KEY (`idDetalle`),
  INDEX `FK_idUsuario_idx` (`idUsuario` ASC) VISIBLE,
  INDEX `fk_Detalle_Plaza_idx` (`idPlaza` ASC) VISIBLE,
  CONSTRAINT `FK_idUsuario`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `EasyParking`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Detalle_Plaza`
    FOREIGN KEY (`idPlaza`)
    REFERENCES `EasyParking`.`Plaza` (`idPlaza`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
