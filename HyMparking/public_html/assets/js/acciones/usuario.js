function listarUsuarios(){
    let peticion=sendRequest('usuario','GET','');
    
    let tabla=document.getElementById("listaUsuarios");
    ///Si la peticion Genera un status 200
    peticion.onload=function(){
        let datos=peticion.response;
        datos.forEach(usuario=>{
            tabla.innerHTML+=`
                <tr>
                    
                    <td>${usuario.cedula}</td>
                    <td>${usuario.nombreCompleto}</td>
                    <td>${usuario.celular}</td>
                    <td></td>
                </tr>
                `
        })
    }
    peticion.onerror=function(){
       alert("error al cargar los datos");
    }       
}

function mostrarFormulario(){
    let modalUsuarios=new bootstrap.Modal(document.getElementById("modalUsuarios"));
    modalUsuarios.show();
}

function guardarUsuario(){
    let cedula=document.getElementById("txtcedula").value;
    let nombre=document.getElementById("txtnombre").value;
    let celular=document.getElementById("txtcelular").value;
  
    let datos={
        "nombreCompleto":nombre ,
        "celular":celular ,
        "cedula": cedula
    };
    let peticion=sendRequest('usuario','POST',datos);
    peticion.onload=function(){
        alert("Usuario Guardado")
        location.reload();
    }
    
    peticion.onerror=function(){
        alert("Error al guardar");
    }
    
}

function BuscarUsuario(){
    let cedula=document.getElementById("txtcedula").value;
    let nombre=document.getElementById("txtnombre").value;
    let celular=document.getElementById("txtcelular").value;
  
    let datos={
        "nombreCompleto":nombre ,
        "celular":celular ,
        "cedula": cedula
    };
    let peticion=sendRequest('usuario','POST',datos);
    peticion.onload=function(){
        alert("Usuario Guardado")
        location.reload();
    }
    
    peticion.onerror=function(){
        alert("Error al guardar");
    }
    
}

