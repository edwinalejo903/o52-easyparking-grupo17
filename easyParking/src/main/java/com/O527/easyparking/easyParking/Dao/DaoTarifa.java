/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.O527.easyparking.easyParking.Dao;

import com.O527.easyparking.easyParking.Modelos.Tarifa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Edwin Hernandez
 */
public interface DaoTarifa extends JpaRepository <Tarifa,Integer> {
    @Query(value="SELECT t.* FROM tarifa t inner join tipovehiculo tv on t.idTipoVehiculo=tv.idTipoVehiculo where tv.idTipoVehiculo= :parametro",nativeQuery=true)
    Tarifa BuscarPorTipoVehiculo(@Param("parametro") int parametro);
    
}
