/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.O527.easyparking.easyParking.Controladores;

import com.O527.easyparking.easyParking.Modelos.Usuario;
import com.O527.easyparking.easyParking.Servicios.ServicioUsuario;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Edwin Hernandez
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/usuario")
public class ControladorUsuario {
    @Autowired
    private ServicioUsuario servicio;
    
    @GetMapping
    public List<Usuario> listar(){
        return servicio.listar();
    }
    
    @PostMapping
    public Usuario  insertar(@RequestBody Usuario Nuevo){
        return servicio.insertar(Nuevo);
    }
    
    @PutMapping
    public Usuario  Actualizar(@RequestBody Usuario Actualizar){
        return servicio.insertar(Actualizar);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Usuario> eliminar(@PathVariable Integer id){
        Usuario objTipo=servicio.buscarPorId(id);
        try {
            servicio.eliminar(id);
             return new ResponseEntity<>(objTipo,HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(objTipo,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @GetMapping("/buscarCedula")
    public Usuario buscarporCedula(@RequestParam String cedula){
        return servicio.buscarPorCedula(cedula);
    }
    
}
