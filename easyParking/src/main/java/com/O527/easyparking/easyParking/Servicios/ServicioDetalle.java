/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.O527.easyparking.easyParking.Servicios;

import com.O527.easyparking.easyParking.Dao.DaoDetalle;
import com.O527.easyparking.easyParking.Modelos.Detalle;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Edwin Hernandez
 */

    
    @Service
public class ServicioDetalle {
    @Autowired
    private DaoDetalle dao;
    
    public Detalle insertar(Detalle Nuevo){
        return dao.save(Nuevo);
    }
    
    public List<Detalle> listar(){
        return dao.findAll();
    }
    
    public Detalle buscarPorId(int id){
        return dao.findById(id).get();
    }
    
    public void eliminar(int id){
        dao.delete(dao.findById(id).get());
    }
    
//    public Detalle buscarPorDetalle(int tipo){
//       return  dao.BuscarPorDetalle(tipo);
//    }


    
}

