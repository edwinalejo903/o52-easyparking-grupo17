/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.O527.easyparking.easyParking.Servicios;

import com.O527.easyparking.easyParking.Dao.DaoTipoVehiculo;
import com.O527.easyparking.easyParking.Modelos.TipoVehiculo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Edwin Hernandez
 */
@Service
public class ServicioTipoVehiculo {
    
    @Autowired
    private DaoTipoVehiculo daoTipoVehiculo;
    
    public TipoVehiculo insertar(TipoVehiculo tipoVehiculoNuevo){
        return daoTipoVehiculo.save(tipoVehiculoNuevo);
    }
    
    public List<TipoVehiculo> listarTipos(){
        return daoTipoVehiculo.findAll();
    }
    
    public TipoVehiculo buscarPorId(int idTipoVehiculo){
        return daoTipoVehiculo.findById(idTipoVehiculo).get();
    }
    
    public void eliminarTipoVehiculo(int idTipoVehiculo){
        daoTipoVehiculo.delete(daoTipoVehiculo.findById(idTipoVehiculo).get());
    }
    
}
