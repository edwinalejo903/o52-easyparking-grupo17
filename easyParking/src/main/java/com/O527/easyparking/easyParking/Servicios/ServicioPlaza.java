/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.O527.easyparking.easyParking.Servicios;

import com.O527.easyparking.easyParking.Modelos.Plaza;
import com.O527.easyparking.easyParking.Dao.DaoPlaza;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author DELL
 */
@Service 
public class ServicioPlaza {
    
    @Autowired
    private DaoPlaza daoPlaza;
    
    public Plaza insertar (Plaza plazaNuevo){
            return daoPlaza.save(plazaNuevo);
    }
    public List<Plaza> listarTipos (){
        return daoPlaza.findAll();
    } 
    public Plaza buscarPorId(int idPlaza){
            return daoPlaza.findById(idPlaza).get();
    }
    
    public void eliminarPlaza(int idPlaza){
        daoPlaza.delete(daoPlaza.findById(idPlaza).get());
    }

    public void eliminar(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Plaza buscarPorTarifa(int tipo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
