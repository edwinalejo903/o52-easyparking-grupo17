/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.O527.easyparking.easyParking.Servicios;

import com.O527.easyparking.easyParking.Dao.DaoUsuario;
import com.O527.easyparking.easyParking.Modelos.Usuario;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Edwin Hernandez
 */
@Service
public class ServicioUsuario {
    @Autowired
    private DaoUsuario dao;
    
    public Usuario insertar(Usuario Nuevo){
        return dao.save(Nuevo);
    }
    
    public List<Usuario> listar(){
        return dao.findAll();
    }
    
    public Usuario buscarPorId(int id){
        return dao.findById(id).get();
    }
    
    public void eliminar(int id){
        dao.delete(dao.findById(id).get());
    }
    
    public Usuario buscarPorCedula(String cedula){
        return dao.findBycedula(cedula);
    }
}
