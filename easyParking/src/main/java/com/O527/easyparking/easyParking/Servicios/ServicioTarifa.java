/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.O527.easyparking.easyParking.Servicios;

import com.O527.easyparking.easyParking.Dao.DaoTarifa;
import com.O527.easyparking.easyParking.Modelos.Detalle;
import com.O527.easyparking.easyParking.Modelos.Tarifa;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Edwin Hernandez
 */
@Service
public class ServicioTarifa {
    @Autowired
    private DaoTarifa dao;
    
    public Tarifa insertar(Tarifa Nuevo){
        return dao.save(Nuevo);
    }
    
    public List<Tarifa> listar(){
        return dao.findAll();
    }
    
    public Tarifa buscarPorId(int id){
        return dao.findById(id).get();
    }
    
    public void eliminar(int id){
        dao.delete(dao.findById(id).get());
    }
    
    public Tarifa buscarPorTipoVehiculo(int tipo){
       return  dao.BuscarPorTipoVehiculo(tipo);
    }

    public Detalle insertar(Detalle Actualizar) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
