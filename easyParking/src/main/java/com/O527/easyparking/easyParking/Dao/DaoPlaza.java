/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.O527.easyparking.easyParking.Dao;

import com.O527.easyparking.easyParking.Modelos.Plaza;
import com.O527.easyparking.easyParking.Modelos.Tarifa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author DELL
 */
public interface DaoPlaza extends JpaRepository<Plaza,Integer>{
    @Query(value="SELECT t.* FROM plaza t inner join tarifa tv on t.idTarifa=tv.idTarifa where tv.idTarifa= :parametro",nativeQuery=true)
    Tarifa BuscarPorTarifa(@Param("parametro") int parametro);
}
