/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.O527.easyparking.easyParking.Modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Edwin Hernandez
 */
@Table
@Entity(name="tarifa")
public class Tarifa implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idtarifa")
    private Integer idTarifa;
    
    
    @Column(name="valorminuto")
    private Integer valorMinuto;
    
    @ManyToOne
    @JoinColumn(name="idtipovehiculo")
    private TipoVehiculo tipovehiculo;

    public Integer getIdTarifa() {
        return idTarifa;
    }

    public void setIdTarifa(Integer idTarifa) {
        this.idTarifa = idTarifa;
    }

    public Integer getValorMinuto() {
        return valorMinuto;
    }

    public void setValorMinuto(Integer valorMinuto) {
        this.valorMinuto = valorMinuto;
    }

    public TipoVehiculo getTipovehiculo() {
        return tipovehiculo;
    }

    public void setTipovehiculo(TipoVehiculo tipovehiculo) {
        this.tipovehiculo = tipovehiculo;
    }
    
    
    
}
