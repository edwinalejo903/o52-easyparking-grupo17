/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.O527.easyparking.easyParking.Dao;

import com.O527.easyparking.easyParking.Modelos.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Edwin Hernandez
 */
public interface DaoUsuario extends JpaRepository<Usuario,Integer>{

    Usuario findBycedula(String cedula);
}