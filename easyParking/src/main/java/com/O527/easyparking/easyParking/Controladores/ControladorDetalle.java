/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.O527.easyparking.easyParking.Controladores;

import com.O527.easyparking.easyParking.Modelos.Detalle;
import com.O527.easyparking.easyParking.Modelos.Plaza;
import com.O527.easyparking.easyParking.Servicios.ServicioDetalle;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Edwin Hernandez
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/detalle")
public class ControladorDetalle {
    @Autowired
    private ServicioDetalle servicio;
    
    @GetMapping
    public List<Detalle> listar(){
        return servicio.listar();
    }
    
    @PostMapping
    public Detalle  insertar(@RequestBody Detalle Nuevo){
        return servicio.insertar(Nuevo);
    }
    
    @PutMapping
    public Detalle  Actualizar(@RequestBody Detalle Actualizar){
        return servicio.insertar(Actualizar);
    }
    
     @DeleteMapping(value="/{id}")
     public ResponseEntity<Detalle> eliminar(@PathVariable Integer id){
        Detalle objTipo=servicio.buscarPorId(id);
        try {
            servicio.eliminar(id);
             return new ResponseEntity<>(objTipo,HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(objTipo,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
//    @GetMapping("/buscar")
//    public Detalle buscarPorTipo(@RequestParam int tipo ){
//        return servicio.buscarPorDetalle(tipo);
//    }
    
 
}


