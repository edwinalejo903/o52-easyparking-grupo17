package com.O527.easyparking.easyParking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyParkingApplication {

	public static void main(String[] args) {
		SpringApplication.run(EasyParkingApplication.class, args);
	}

}
