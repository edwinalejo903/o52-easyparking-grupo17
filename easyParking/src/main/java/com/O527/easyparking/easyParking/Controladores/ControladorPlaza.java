/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.O527.easyparking.easyParking.Controladores;

import com.O527.easyparking.easyParking.Modelos.Plaza;
import com.O527.easyparking.easyParking.Servicios.ServicioPlaza;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author DELL
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/plaza")
public class ControladorPlaza {
    
    @Autowired
    private ServicioPlaza servicio;
    
    @GetMapping
    public List<Plaza> listarTipos(){
        return servicio.listarTipos();
    }
    
    @PostMapping
    public Plaza  insertar(@RequestBody Plaza Nuevo){
        return servicio.insertar(Nuevo);
    }
    
    @PutMapping
    public Plaza  Actualizar(@RequestBody Plaza Actualizar){
        return servicio.insertar(Actualizar);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Plaza> eliminar(@PathVariable Integer id){
        Plaza objTipo=servicio.buscarPorId(id);
        try {
            servicio.eliminar(id);
             return new ResponseEntity<>(objTipo,HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(objTipo,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @GetMapping("/buscar")
    public Plaza buscarPorTipo(@RequestParam int tipo ){
        return servicio.buscarPorTarifa(tipo);
    }

}
